#!/usr/bin/env bash

set -eux

function remove_system_docker {
  dnf remove docker \
             docker-client \
             docker-client-latest \
             docker-common \
             docker-latest \
             docker-latest-logrotate \
             docker-logrotate \
             docker-selinux \
             docker-engine-selinux \
             docker-engine
}

function add_docker_repo {
  dnf install -y dnf-plugins-core

  dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
}

function install_docker {
  dnf install -y docker-ce docker-ce-cli containerd.io
}

function enable_docker {
  systemctl start docker
  systemctl enable docker
}

function add_docker_group {
  groupadd docker
  usermod -aG docker vagrant
}

function install_docker_compose {
  local compose_version=${DOCKER_COMPOSE_VERSION:-v2.4.1}
  mkdir -p /usr/lib/docker/cli-plugins

  curl -L "https://github.com/docker/compose/releases/download/${compose_version}/docker-compose-linux-x86_64" -o /usr/lib/docker/cli-plugins/docker-compose
  chmod +x /usr/lib/docker/cli-plugins/docker-compose
}

remove_system_docker
add_docker_repo
install_docker
enable_docker
install_docker_compose
