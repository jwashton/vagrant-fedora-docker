source "vagrant" "fedora-docker" {
  add_force = true
  communicator = "ssh"
  provider = "libvirt"
  source_path = "fedora/35-cloud-base"
  template = "./Vagrantfile"
}

build {
  sources = ["source.vagrant.fedora-docker"]

  provisioner "shell" {
    execute_command = "echo 'vagrant' | {{.Vars}} sudo -S -E bash '{{.Path}}'"
    script = "./setup.sh"
  }
}